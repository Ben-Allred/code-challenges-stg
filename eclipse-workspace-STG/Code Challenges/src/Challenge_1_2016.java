import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Challenge_1_2016 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\development\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		//Go to site and get Title
		driver.navigate().to("https://www.skiutah.com");
		String actualTitle = driver.getTitle();
		System.out.println(actualTitle); //Ski Utah | Utah Ski Resorts, Lift Tickets, Ski Passes, Maps & More! - Ski Utah
		
		String expectedTitle = "Ski Utah";
		
		//String words = actualTitle.substring(0, 8);  //Original working code
		
		//New code to split the string without hard coded values
		String[] words = actualTitle.split("- ");
		String newTitle = words[1]; // "Ski Utah"
		System.out.println(newTitle);
		//String part1 = words[0]; // "Start of Title String
		
		assert newTitle.equals(expectedTitle);
		
		if (newTitle.equals(expectedTitle))
		{
			System.out.println("Test case Pass!");
		}
		else
		{
			System.out.println("Test Case Fail!");
		}
		driver.close();
	}

}
