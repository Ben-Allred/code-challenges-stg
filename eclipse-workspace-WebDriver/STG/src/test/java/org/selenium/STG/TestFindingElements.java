package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFindingElements {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.skiutah.com");
		
		WebElement label;// = driver.findElement(By.xpath("//*[@id='logo']/div/svg"));
		label = driver.findElement(By.cssSelector(".RFjuSb.bxPAYd.k6Zj8d"));
		assert label.isDisplayed();
		
		WebElement headerText = driver.findElement(By.id("headingText"));
		assert headerText.isDisplayed();
		
		WebElement headerSubtext = driver.findElement(By.id("headingSubtext"));
		assert headerSubtext.isDisplayed();
		
		//Directly access and act on a web element on the same line
		WebElement email = driver.findElement(By.xpath("//*[@id='identifierId']"));
		email.sendKeys("engelmoroni@gmail.com");
		
		WebElement forgot = driver.findElement(By.cssSelector(".uBOgn"));
		assert forgot.isDisplayed();
		forgot.click();
		
		WebElement headerText2 = driver.findElement(By.id("headingText"));
		headerText2.getText();
		System.out.println(headerText2);
		assert headerText2.isDisplayed();
		
		driver.navigate().back();
		
		//Directly access and act on a web element on the same line
		WebElement more = driver.findElement(By.xpath("//*[@id='ow210']"));
		more.click();
		
		WebElement signup = driver.findElement(By.xpath("//*[@id='SIGNUP']"));
		signup.isDisplayed();
		
		email.click();
		
		driver.close();
	}

}
