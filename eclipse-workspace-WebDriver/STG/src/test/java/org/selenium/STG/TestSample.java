package testcases;

//import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//import com.gargoylesoftware.htmlunit.javascript.host.file.File;
//import com.mysql.cj.x.protobuf.MysqlxConnection.Capability;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class TestSample {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\development\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.navigate().to("https://www.skiutah.com");
		String actualTitle = driver.getTitle(); //From website
		System.out.println(actualTitle);
		String expectedTitle = "Ski Utah";// | Utah Ski Resorts, Lift Tickets, Ski Passes, Maps & More! - Ski Utah";
		
		//int length = driver.getTitle().length();
		
		//String splitChar = "|";
		
		String words = actualTitle.substring(0, 8);
		System.out.println(words);
        
//        for(int i=0; i<length; i++)
//        {                
//        	//System.out.println(words);
//       }

		//System.out.println(driver.getTitle().length());
		
		//driver.manage().window().maximize();
		//actualTitle.equals(expectedTitle);
		
		if (words.equals(expectedTitle))
		{
			System.out.println("Test case Pass!");
		}
		else
		{
			System.out.println("Test Case Fail!");
		}
		
		driver.close();
		//driver.quit();
	}

}
