package org.openqa.selenium.firefox;

import org.openqa.selenium.WebDriver;

//This will be the same for each of the other browsers
public class FirefoxDriver implements WebDriver
{

	public FirefoxDriver()
	{
		System.out.println("Launch the Firefox Browser");  //Would change for Firefox or IE to that browser
	}
	
	public void get(String url)
	{
		System.out.println("Navigate to the URL in Firefox");
	}
	
	public void getTitle()
	{
		System.out.println("Getting the Title from the current URL in Firefox");
	}
	
	public void click()
	{
		System.out.println("Clicking an element in Firefox");
	}
	
	public void sendKeys()
	{
		System.out.println("Send Text to an element in Firefox");
	}
	
	public static void main(String[] args)
	{
		FirefoxDriver driver = new FirefoxDriver();
		driver.get("http://www.google.com");
	}
}
