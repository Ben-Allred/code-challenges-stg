package org.openqa.selenium.edge;

import org.openqa.selenium.WebDriver;

//This will be the same for each of the other browsers
public class EdgeDriver implements WebDriver
{

	public EdgeDriver()
	{
		System.out.println("Launch the Edge Browser");  //Would change for Firefox or IE to that browser
	}
	
	public void get(String url)
	{
		System.out.println("Navigate to the URL in Edge");
	}
	
	public void getTitle()
	{
		System.out.println("Getting the Title from the current URL in Edge");
	}
	
	public void click()
	{
		System.out.println("Clicking an element in Edge");
	}
	
	public void sendKeys()
	{
		System.out.println("Send Text to an element in Edge");
	}
	
	public static void main(String[] args)
	{
		EdgeDriver driver = new EdgeDriver();
		driver.get("http://www.google.com");
	}
}
