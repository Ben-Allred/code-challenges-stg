package org.openqa.selenium;

public interface WebDriver {

	public void getTitle();
	public void get(String url);
	public void click();
	public void sendKeys();
}
