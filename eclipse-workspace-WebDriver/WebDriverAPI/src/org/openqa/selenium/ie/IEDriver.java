package org.openqa.selenium.ie;

import org.openqa.selenium.WebDriver;

//This will be the same for each of the other browsers
public class IEDriver implements WebDriver
{

	public IEDriver()
	{
		System.out.println("Launch the IE Browser");  //Would change for Firefox or IE to that browser
	}
	
	public void get(String url)
	{
		System.out.println("Navigate to the URL in IE");
	}
	
	public void getTitle()
	{
		System.out.println("Getting the Title from the current URL in IE");
	}
	
	public void click()
	{
		System.out.println("Clicking an element in IE");
	}
	
	public void sendKeys()
	{
		System.out.println("Send Text to an element in IE");
	}
	
	public static void main(String[] args)
	{
		IEDriver driver = new IEDriver();
		driver.get("http://www.google.com");
	}
}
