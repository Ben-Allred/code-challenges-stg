package org.openqa.selenium.chrome;

import org.openqa.selenium.WebDriver;

//This will be the same for each of the other browsers
public class ChromeDriver implements WebDriver
{

	public ChromeDriver()
	{
		System.out.println("Launch the Chrome Browser");  //Would change for Firefox or IE to that browser
	}
	
	public void get(String url)
	{
		System.out.println("Navigate to the URL in Chrome");
	}
	
	public void getTitle()
	{
		System.out.println("Getting the Title from the current URL in Chrome");
	}
	
	public void click()
	{
		System.out.println("Clicking an element in Chrome");
	}
	
	public void sendKeys()
	{
		System.out.println("Send Text to an element in Chrome");
	}
	
	public static void main(String[] args)
	{
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
	}
}
