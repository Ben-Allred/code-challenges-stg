//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;

public class TestSample {

	public static void main(String[] args) {

		//CTRL + SHIFT + O - will import the library (The package containing the class)
		
		//Setting up the Gecko Driver for Firefox use.
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\bnallred\\Downloads\\geckodriver.exe");
		//FirefoxDriver FFdriver = new FirefoxDriver();
		
		//MicrosoftWebDriver edgeDriver new MicrosoftWebDriver();
		
		/*
		//Setup Chrome driver
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\bnallred\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		driver.close();  //Closes only the current window
		//driver.quit();  //Closes all open windows
		*/
		
		/*
		//Setup IE driver - IEDriverServer.exe  -  Security must be at the same level (?Medium?)
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		WebDriver driver = new InternetExplorerDriver(capabilities);  //When defining capabilities the capability must be provided to the class
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		driver.close();  //Closes only the current window
		driver.quit();
		*/
		
		/*
		//Setup edge driver - IEDriverServer.exe  -  Security must be at the same level (?Medium?)
		EdgeDriver driver = new EdgeDriver();
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		driver.close();  //Closes only the current window
		*/
	}

}
